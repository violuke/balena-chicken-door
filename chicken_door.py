import os

from astral import LocationInfo

from door_interface import DoorInterface

print("Starting chicken door app")

# Config
DEBUG = os.environ.get("DEBUG", "0") == "1"
MAX_OPEN_CLOSE_MINS = int(os.environ.get("MAX_OPEN_CLOSE_MINS", "2"))

# For these, positive minutes = after and negative minutes = before
OPEN_MINS_RELATIVE_TO_SUNRISE = int(os.environ.get("OPEN_MINS_RELATIVE_TO_SUNRISE", "0"))
CLOSE_MINS_RELATIVE_TO_SUNSET = int(os.environ.get("CLOSE_MINS_RELATIVE_TO_SUNSET", "30"))
OVER_STEPS_ON_CLOSE_TO_LOCK = int(os.environ.get("OVER_STEPS_ON_CLOSE_TO_LOCK", "250"))

city = os.environ.get("CITY", "London")
region = os.environ.get("REGION", "England")
tz = os.environ.get("TZ", "Europe/London")
lat = float(os.environ.get("LAT", "51.5"))
lon = float(os.environ.get("LON", "-0.116"))

# Run our program
door_interface = DoorInterface(
    debug=DEBUG,
    open_mins_relative_to_sunrise=OPEN_MINS_RELATIVE_TO_SUNRISE,
    close_mins_relative_to_sunset=CLOSE_MINS_RELATIVE_TO_SUNSET,
    max_open_close_mins=MAX_OPEN_CLOSE_MINS,
    over_steps_on_close_to_lock=OVER_STEPS_ON_CLOSE_TO_LOCK,
    city=LocationInfo(city, region, tz, lat, lon),
)

door_interface.go()
