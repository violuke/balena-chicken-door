from time import sleep
from datetime import datetime, timedelta, timezone

from adafruit_motor import stepper
from astral import LocationInfo
from astral.sun import sun

from adafruit_motorkit import MotorKit
import RPi.GPIO as GPIO


class DoorInterface(object):
    DOOR_STATE_OPEN = 1
    DOOR_STATE_CLOSED = 2

    # TODO: are these the right way around?
    PIN_OPEN_SWITCH = 17
    PIN_CLOSED_SWITCH = 27

    def __init__(self, debug: bool, open_mins_relative_to_sunrise: int, close_mins_relative_to_sunset: int, max_open_close_mins: int, over_steps_on_close_to_lock: int, city: LocationInfo):
        self.debug = debug
        self.open_mins_relative_to_sunrise = open_mins_relative_to_sunrise
        self.close_mins_relative_to_sunset = close_mins_relative_to_sunset
        self.max_open_close_mins = max_open_close_mins
        self.over_steps_on_close_to_lock = over_steps_on_close_to_lock
        self.city = city
        self.door_state = None

        # Prepare motorkit for usage
        self.kit = MotorKit()

        # Prepare limit switches
        # GPIO.setwarnings(False)
        # GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.PIN_OPEN_SWITCH, GPIO.IN,)  # , pull_up_down=GPIO.PUD_DOWN
        GPIO.setup(self.PIN_CLOSED_SWITCH, GPIO.IN)  # , pull_up_down=GPIO.PUD_DOWN

    def __open_door(self):
        self.__control_door(
            direction=stepper.FORWARD,
            limit_function=self.__fully_open_limit_switch_engaged,
            direction_text="OPEN"
        )

    def __close_door(self):
        self.__control_door(
            direction=stepper.BACKWARD,
            limit_function=self.__fully_closed_limit_switch_engaged,
            direction_text="CLOSED"
        )

        # After closing (closing only) move an extra 50 steps to let the lock lock
        i = 0
        while i < self.over_steps_on_close_to_lock:
            i += 1
            self.kit.stepper1.onestep(style=stepper.DOUBLE, direction=stepper.BACKWARD)

        # Save energy
        self.kit.stepper1.release()

    def __control_door(self, direction, limit_function, direction_text: str):
        if self.debug:
            print("About to move door to " + direction_text + " position")

        # Record when we started, so we can detect faults
        start = datetime.now()

        while True:
            # Check limit switch
            if limit_function():
                if self.debug:
                    print("Door fully moved to " + direction_text + " position, according to limit switch")
                break

            # Move door
            self.kit.stepper1.onestep(style=stepper.DOUBLE, direction=direction)

            # Check we've not been moving for a problem-indicating time
            if start + timedelta(minutes=self.max_open_close_mins) < datetime.now():
                if self.debug:
                    print("Door has been moving to " + direction_text + " position for " + str(self.max_open_close_mins) + " mins - aborting!")
                break

        if self.debug:
            print("Door successfully moved to " + direction_text + " position")

        # Set door state
        if direction == stepper.FORWARD:
            self.door_state = self.DOOR_STATE_OPEN
        else:
            self.door_state = self.DOOR_STATE_CLOSED

    def __fully_open_limit_switch_engaged(self):
        if GPIO.input(self.PIN_OPEN_SWITCH) != GPIO.HIGH:
            print("Door fully open")
            return True
        else:
            # print("Door is not fully open")
            return False

    def __fully_closed_limit_switch_engaged(self):
        if GPIO.input(self.PIN_CLOSED_SWITCH) != GPIO.HIGH:
            print("Door fully closed")
            return True
        else:
            # print("Door is not fully open")
            return False

    def go(self):
        if self.debug:
            print("Starting chicken door monitor")

        # Normalise door position
        self.__normalise_door_position()

        # Loop forever...
        while True:
            # When is sunrise/sunset
            now = datetime.now(timezone.utc)
            s = sun(self.city.observer, date=now.date())

            sunrise = s["sunrise"] + timedelta(minutes=self.open_mins_relative_to_sunrise)
            sunset = s["sunset"] + timedelta(minutes=self.close_mins_relative_to_sunset)

            if self.door_state == self.DOOR_STATE_OPEN:
                # Door currently open
                if now > sunset:
                    # Past closing point, close door now
                    if self.debug:
                        print("Now is " + str(now))
                        print("Sunset is " + str(s["sunset"]))
                        print("We close at sunset + " + str(self.close_mins_relative_to_sunset) + " min, so closing now")

                    self.__close_door()

                else:
                    print("Door open. In UTC it's " + str(now) + " and we'll close at " + str(sunset) + "...")

            else:
                # Door currently closed
                if sunrise < now < sunset:
                    # Past closing point, close door now
                    if self.debug:
                        print("Now is " + str(now))
                        print("Sunrise is " + str(s["sunrise"]))
                        print("We open at sunrise + " + str(self.close_mins_relative_to_sunset) + " min, so opening now")

                    self.__open_door()

                else:
                    print("Door closed. In UTC it's " + str(now) + " and we'll open at " + str(sunrise) + "...")

            # Sleep for a minute!
            sleep(60)

    def __normalise_door_position(self):
        # This is called when first getting going to set the initial door position

        # When is sunrise/sunset
        now = datetime.now(timezone.utc)
        s = sun(self.city.observer, date=now.date())

        # Are we already hitting a limit switch?
        if self.__fully_open_limit_switch_engaged():
            self.door_state = self.DOOR_STATE_OPEN
            return

        if self.__fully_closed_limit_switch_engaged():
            self.door_state = self.DOOR_STATE_CLOSED
            return

        # Not fully open or fully closed, move the door somewhere to get started
        print("Door not detected as fully opened or fully closed at start...")
        print("Opening door as a starting point")
        self.__open_door()
